### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ cedaf8a0-caae-11ec-1e4a-93330cd20312
using PlutoUI

# ╔═╡ f7508e90-cc2d-483a-b3ad-49aabcb6e2c0
using Setfield

# ╔═╡ 9b6fbb6c-a9c0-4478-99dc-7b2a6cef660a
md""" Getting user input """

# ╔═╡ 6b429dbd-6c14-46fc-8e46-5c7dffaa1f68
@bind floorNumbers TextField()

# ╔═╡ 4bd43e72-eddf-416e-a694-006c6a371404
floor = (floorNumbers =="" ? "" : floorNumbers)

# ╔═╡ e7a3e21d-0546-463c-8eda-6b96b878af54
md""" Offices per floor """

# ╔═╡ 0d41174f-b409-4158-bed2-bdb8fc36dac0
@bind officesfloor TextField()

# ╔═╡ 96462ec1-ac38-46a9-95f8-4e32222d3078
officesPerFloor = officesfloor

# ╔═╡ 848566f4-7d4a-4a52-8efd-fbf0fbad6a5a
md""" #### Creating an office """

# ╔═╡ 00bb084c-9f93-4b50-9ef2-0b767eecc275
	struct Office
		officeNumber::Tuple{Int64,Int64}
		parcels::Int64
		floorNumber::Int64
	end

# ╔═╡ 762e8b97-4453-4dca-a3ba-b61196ec20b9
@bind officeNumber confirm(TextField())

# ╔═╡ add54193-6e99-404c-89c4-34dd938f9904
@bind floorNumber confirm(TextField())

# ╔═╡ a009c65e-b019-4b04-998a-6bbce53f1430
@bind parcels confirm(TextField())

# ╔═╡ bc37c2d5-5415-4b0b-b952-fc07c19f00cf
@bind submit Button("Create")

# ╔═╡ 146e5b70-b436-4c3a-b9be-17a406f93457
submit

# ╔═╡ 983befaa-aff7-4c8b-be8c-5a296d84e72e
begin
officeFloor = floorNumber
officeParcels = parcels
officeID = officeNumber
end

# ╔═╡ 14ff2b56-5ae8-4631-8f02-fe20ffa4b87f
function createOffice(floorNo,parcels,officeNumber)
	return office = Office(officeNumber,parcels,floorNo)
end

# ╔═╡ 26899591-1dd5-48b5-9151-f669213c8f31
md""" ### Defining the state """

# ╔═╡ 37e06f11-39de-4a0a-bd55-4b4bfa258af1
# struct State
# 	offices::Vector{Office}
# 	agentPosition::Tuple{Int64,Int64}
# 	carrying::Bool
# 	parcels::Vector{Int64}
# end

# ╔═╡ a1c79b8b-a8e6-4967-9df4-4e225b73e77e
struct StateFinal
	office::Office
	location::Tuple{Int64,Int64}
	carrying::Bool
	parcels::Int64
end

# ╔═╡ 8135c32c-4b6a-4352-a0dc-1a3c59a86e29
md""" ### Creating Actions"""

# ╔═╡ d0fcf133-e70a-4723-a1a3-bedfe0f399db
struct Action
	name::String
	cost::Int64
end

# ╔═╡ d48dd135-1da8-4a37-87e9-1849391b41e3
begin
moveEast = Action("ME",2)
moveWest = Action("MW",2)
moveUp = Action("MU",1)
moveDown = Action("MD",1)
collect = Action("CO",5)
end

# ╔═╡ 2a036bb6-88c9-432b-bdbc-7b3f096ee1c3
md""" ##### Creating Transition model """

# ╔═╡ 0379eb5c-d831-4d6e-8623-4b3cd4363698
TranstionModel = Dict{StateFinal,Vector}()

# ╔═╡ 10bc40a3-e111-4445-8938-fca18bc3c191
md""" ###### A function that creates 2 states per office then add then to the transitionModel """

# ╔═╡ eb2ae71d-d35d-443e-b746-322f24c34b0a
md""" ### Functions for Actions """

# ╔═╡ 3dc194c9-f1fc-4db1-a2e3-d9182753b46f
function up(offices::Matrix,state::StateFinal)::StateFinal

	println(".............In Method UP.................")
	
	rows = size(offices,1)
	#println("--------------Number of rows-----------------")
	#println(rows)
		
	location = state.location
    println("--------Prev Location---------")
	println(location)
	println("")
	
	if location[1]-1 <= 0
		println(".....  WE CAN'T MOVE UP IN UP()........")
		println(".....  EXITING METHOD UP()........")
		#Then we can't move up
		return state
	else
		#We change the location of the state
		println(".....  WE can MOVE UP IN UP() ........")
		location = (location[1]-1,location[2])
		println("--------New Location---------")
		println(location)
		newState = StateFinal(state.office,location,state.carrying,state.parcels)
		#@set newState.location = location
		print("-----------------UP METHOD STATE -------------")
		println(newState)
		print("")
		print("............EXITING UP METHOD .....................")
		#newState.location = location
		return newState
	end
		
end

# ╔═╡ 6539c620-3e7a-40d1-baa4-bcc82cf30d67
function down(offices::Matrix,state::StateFinal)::StateFinal
	println("............IN THE DOWN METHOD().....................")
	rows = size(offices,1)
	location = state.location
	println("............THE LOCATION IS .....................")
	println(location)
	println("")
	
	if location[1]+1 > rows
		println("............WE CAN'T MOVE DOWN() .....................")
		println("............EXIT DOWN().....................")
		#Then we can't move down
		return state
	else
		#We change the location of the state
		println("............WE CAN MOVE DOWN .....................")
		location = (location[1] +1,location[2])
		newState = StateFinal(state.office,location,state.carrying,state.parcels)
		println("............THE STATE RETURNED IN DOWN().....................")
		println(newState)
		println("............EXTING METHOD DOWN() .....................")
		println("")
		#@set newState.location = location
		# newState.location = location
		return newState
	end
		
end

# ╔═╡ b29b353b-3c72-4338-8126-10358fc89f8a
function east(offices::Matrix,state::StateFinal)::StateFinal
	println("............IN THE EAST() .....................")
	cols = size(offices,2)
	location = state.location
	println("THE LOCATION IN EAST")
	println(location)
	
	if location[2]+1 > cols
		#Then we can't move up
		println("............WE CAN't MOVE EAST .....................")
		println("............EXITING EAST METHOD .....................")
		return state
	else
		println("............WE CAN MOVE EAST .....................")
		#We change the location of the state
		location = (location[1],location[2]+1)
		newState = StateFinal(state.office,location,state.carrying,state.parcels)
		println("............THE STATE IN EAST .....................")
		println(newState)
		println("............EXITING  EAST() .....................")
		#@set newState.location = location
		# newState.location = location
		return newState
	end
		
end

# ╔═╡ e9b57036-dbe2-4d60-81d4-18e88e214916
function west(offices::Matrix,state::StateFinal)::StateFinal
	println("...........IN WEST().....................")
	cols = size(offices,2)
	location = state.location
	
	if location[2]-1 <= 0
		println("........... WE CAN'T MOVE WEST().....................")
		println("........... EXITING WEST().....................")
		#Then we can't move up
		return state
	else
		println("........... WE CAN MOVE WEST().....................")
		#We change the location of the state
		location = (location[1],location[2]-1)
		newState = StateFinal(state.office,location,state.carrying,state.parcels)
		println("........... NEW STATE IN WEST().....................")
		println(newState)
		println("Exiting WEST()")
		println("")
		#@set newState.location = location
		return newState
	end
		
end

# ╔═╡ 28d7b7e1-6717-478b-aea8-044f9d8c9978
function pickUp(offices::Matrix,state::StateFinal)::StateFinal
	println("*********** IN METHOD PICKUP()")

	if state.carrying == true
		println("*********** AGENT IS ALREADY CARRYING, EXIT METHOD PICKUP() *****")
		#Then we can not pick up 
		return state
	else
		println("*********** AGENT IS NOT CARRYING, MAKE NEW STATE *****")
		newState = StateFinal(state.office,state.location,true,state.parcels)
		#We pick up the parcel
		#@set state.carrying = true
		# state.carrying = true
		println("*********** EXIT PICKUP()")
		return newState
	end
		
end

# ╔═╡ 713b9809-ca45-407d-bb30-f2db050a8063
function createStates(floorNumber::Int64,roomNumbers::Int64,numberOfParcels::Int64)::StateFinal
	#Create an array with (floorNumber,roomNumbers)
	offices = rand(floorNumber,roomNumbers)
	println(offices);
	
		for (i, j) in Tuple.(CartesianIndices(offices))
			
			#Getting the CartesianIndex of an element
     		location = (i,j)
			println("The location is: *************************************|")
			println(location)
			#println(typeof(offices))
			
			#for k in offices
			#Creating the office
			office = Office(location,numberOfParcels,floorNumber)
			#Creating the states were the parcel is picked up and not
		    state1 = StateFinal(office,location,false,numberOfParcels)
			state2 = StateFinal(office,location,true,numberOfParcels)

			#Checking if state in not NULL
			if state1 != nothing
			#Add state1 to transition model
			push!(TranstionModel,state1 =>[(moveUp,up(offices,state1)),(moveDown,down(offices,state1)),(moveEast,east(offices,state1)),(moveWest,west(offices,state1)),(collect,pickUp(offices,state1))])
			end

			if state2 != nothing
				
			#Add state2 to transition model
			push!(TranstionModel,state2 =>[(moveUp,up(offices,state2)),(moveDown,down(offices,state2)),(moveEast,east(offices,state2)),(moveWest,west(offices,state2)),(collect,pickUp(offices,state2))])
			#end
			end

			
       end

	
end

# ╔═╡ 0652c7cf-9601-4b94-91a3-0eeea864c21a
createStates(2,3,1)

# ╔═╡ 70100fa2-73b4-49ee-a8b1-7906b0355645
TranstionModel

# ╔═╡ acc7c4d1-57c3-4d8c-b9d7-3bd4d9caa053
md""" #### Write the A* search function to search the transition model dict """

# ╔═╡ 39ea8bff-b636-4e92-9ca9-15341420d838
function aStar()
	
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Setfield = "efcf1570-3423-57d1-acb7-fd33fddbac46"

[compat]
PlutoUI = "~0.7.38"
Setfield = "~0.8.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Setfield]]
deps = ["ConstructionBase", "Future", "MacroTools", "Requires"]
git-tree-sha1 = "38d88503f695eb0301479bc9b0d4320b378bafe5"
uuid = "efcf1570-3423-57d1-acb7-fd33fddbac46"
version = "0.8.2"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═cedaf8a0-caae-11ec-1e4a-93330cd20312
# ╠═f7508e90-cc2d-483a-b3ad-49aabcb6e2c0
# ╠═9b6fbb6c-a9c0-4478-99dc-7b2a6cef660a
# ╠═6b429dbd-6c14-46fc-8e46-5c7dffaa1f68
# ╠═4bd43e72-eddf-416e-a694-006c6a371404
# ╠═e7a3e21d-0546-463c-8eda-6b96b878af54
# ╠═0d41174f-b409-4158-bed2-bdb8fc36dac0
# ╠═96462ec1-ac38-46a9-95f8-4e32222d3078
# ╠═848566f4-7d4a-4a52-8efd-fbf0fbad6a5a
# ╠═00bb084c-9f93-4b50-9ef2-0b767eecc275
# ╠═762e8b97-4453-4dca-a3ba-b61196ec20b9
# ╠═add54193-6e99-404c-89c4-34dd938f9904
# ╠═a009c65e-b019-4b04-998a-6bbce53f1430
# ╠═bc37c2d5-5415-4b0b-b952-fc07c19f00cf
# ╠═146e5b70-b436-4c3a-b9be-17a406f93457
# ╠═983befaa-aff7-4c8b-be8c-5a296d84e72e
# ╠═14ff2b56-5ae8-4631-8f02-fe20ffa4b87f
# ╠═26899591-1dd5-48b5-9151-f669213c8f31
# ╠═37e06f11-39de-4a0a-bd55-4b4bfa258af1
# ╠═a1c79b8b-a8e6-4967-9df4-4e225b73e77e
# ╠═8135c32c-4b6a-4352-a0dc-1a3c59a86e29
# ╠═d0fcf133-e70a-4723-a1a3-bedfe0f399db
# ╠═d48dd135-1da8-4a37-87e9-1849391b41e3
# ╠═2a036bb6-88c9-432b-bdbc-7b3f096ee1c3
# ╠═0379eb5c-d831-4d6e-8623-4b3cd4363698
# ╟─10bc40a3-e111-4445-8938-fca18bc3c191
# ╠═713b9809-ca45-407d-bb30-f2db050a8063
# ╠═eb2ae71d-d35d-443e-b746-322f24c34b0a
# ╠═3dc194c9-f1fc-4db1-a2e3-d9182753b46f
# ╠═6539c620-3e7a-40d1-baa4-bcc82cf30d67
# ╠═b29b353b-3c72-4338-8126-10358fc89f8a
# ╠═e9b57036-dbe2-4d60-81d4-18e88e214916
# ╠═28d7b7e1-6717-478b-aea8-044f9d8c9978
# ╟─0652c7cf-9601-4b94-91a3-0eeea864c21a
# ╠═70100fa2-73b4-49ee-a8b1-7906b0355645
# ╠═acc7c4d1-57c3-4d8c-b9d7-3bd4d9caa053
# ╠═39ea8bff-b636-4e92-9ca9-15341420d838
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
